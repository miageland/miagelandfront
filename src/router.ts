import {createRouter, createWebHistory, RouteRecordRaw} from 'vue-router'
import Login from "./views/Login.vue";
import Dashboard from "./views/Dashboard.vue";
import Employees from "./views/Employees.vue";
import Tickets from "./views/Tickets.vue";
import MyTickets from "./views/MyTickets.vue";
import Attractions from "./views/Attractions.vue";
import Generator from "./views/Generator.vue";
import authenticationService from "./services/AuthenticationService.ts";

const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        redirect: '/dashboard'
    },
    {
        path: '/dashboard',
        name: 'Dashboard',
        component: Dashboard,
        meta: {requiresAuth: true}
    },
    {
        path: '/employees',
        name: 'Employees',
        component: Employees,
        meta: {requiresAuth: true}
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/tickets',
        name: 'Tickets',
        component: Tickets,
        meta: {requiresAuth: true}
    },
    {
        path: '/mytickets',
        name: 'myTickets',
        component: MyTickets,
        meta: {requiresAuth: true}
    },
    {
        path: '/attractions',
        name: 'Attractions',
        component: Attractions,
        meta: {requiresAuth: true}
    },
    {
        path: '/generator',
        name: 'Generator',
        component: Generator,
        meta: {requiresAuth: true}
    },
]

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes
})

router.beforeEach((to) => {
    if (!authenticationService.isLoggedIn() && to.name !== 'Login') {
        return {name: 'Login'}
    }
});

export default router
