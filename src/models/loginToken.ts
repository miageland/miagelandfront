export class LoginToken {
  constructor(
    public id: number,
    public isEmployee: boolean
  ) {}
}
