export class Attraction {
  constructor(
    public name: string,
    public isOpened: boolean,
    public isOpenedDisplay?: string,
    public id?: number
  ) {}
}