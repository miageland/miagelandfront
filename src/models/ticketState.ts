export enum TicketState {
    notPAID = "notPAID",
    PAID = "PAID",
    USED = "USED",
    CANCELLED = "CANCELLED"
}
