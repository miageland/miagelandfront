import {Role} from "./role.ts";

export class Employee {
  constructor(
    public lastName: string,
    public firstName: string,
    public password: string,
    public mail: string,
    public role: Role,
    public roleDisplay?: string,
    public id?: number
  ) {}
}

