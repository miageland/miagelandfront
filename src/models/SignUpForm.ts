export class SignUpForm {
  constructor(
    public mail: string = "",
    public password: string = "",
    public lastName: string = "",
    public firstName: string = ""
  ) {}
}