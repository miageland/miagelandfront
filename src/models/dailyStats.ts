export class DailyStats {
  constructor(
    public date: Date,
    public ticketsSold: number,
    public visitorsCount: number,
    public income: number
  ) {}
}
