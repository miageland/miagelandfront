import {GeneralStatsCategory} from "./generalStatsCategory.ts";

export class GeneralStats {
  constructor(
    public category: GeneralStatsCategory,
    public amount: number
  ) {}
}
