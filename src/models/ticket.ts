import {Visitor} from "./visitor.ts";
import {TicketState} from "./ticketState.ts";

export class Ticket {
    constructor(
        public visitDate: Date,
        public price: number,
        public state: TicketState,
        public visitor?: Visitor,
        public visitDateDisplay?: string,
        public priceDisplay?: string,
        public stateDisplay?: string,
        public ticketNumber?: number
    ) {
    }
}

