export class Visitor {
  constructor(
    public lastName: string,
    public firstName: string,
    public password: string,
    public mail: string,
    public id?: number
  ) {}
}
