import {createApp} from 'vue'
import App from './App.vue'
import router from "./router.ts";
import { setupCalendar, Calendar, DatePicker } from 'v-calendar';
import 'v-calendar/style.css';


import './style.css';


const app = createApp(App)
app.use(router).use(setupCalendar, {}).component('VCalendar', Calendar)
    .component('VDatePicker', DatePicker)
app.mount('#app')
