import axios from 'axios';

const API_URL = 'http://localhost:8080';

const DataLoaderService = {
    async firstRun(): Promise<void> {
        try {
            await axios.get(`${API_URL}/dataLoad`);
            return;
        } catch (error) {
            throw error;
        }
    },
};

export default DataLoaderService;
