import axios, {AxiosResponse} from 'axios';
import {Ticket} from "../models/ticket.ts";
import {DateService} from "./dateService.ts";
import {Visitor} from "../models/visitor.ts";
import {TicketState} from "../models/ticketState.ts";

const API_URL = 'http://localhost:8080';


const TicketService = {

    formatTicketForDisplay(ticket: Ticket) {
        ticket.visitDateDisplay = DateService.formatDateToFrench(new Date(ticket.visitDate));
        ticket.priceDisplay = ticket.price + " €";
        switch (ticket.state) {
            case TicketState.CANCELLED:
                ticket.stateDisplay = "Annulé";
                break;

            case TicketState.notPAID:
                ticket.stateDisplay = "En attente de paiement";
                break;

            case TicketState.PAID:
                ticket.stateDisplay = "Payé";
                break;

            case TicketState.USED:
                ticket.stateDisplay = "Utilisé";
                break;

        }
        return ticket;
    },

    async getTickets(): Promise<Ticket[]> {
        try {
            const response: AxiosResponse<Ticket[]> = await axios.get(`${API_URL}/tickets`);
            for (let ticket of response.data) {
                ticket = this.formatTicketForDisplay(ticket);
            }
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    async getVisitorsTickets(visitor: Visitor): Promise<Ticket[]> {
        try {
            const response: AxiosResponse<Ticket[]> = await axios.get(`${API_URL}/visitors/` + visitor.id
                + '/tickets');
            for (let ticket of response.data) {
                ticket = this.formatTicketForDisplay(ticket);
            }
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    getVisitorsVisitsAmount(tickets: Ticket[]) {
        return tickets.filter(ticket => ticket.state === TicketState.USED).length;
    },

    async createTicket(ticket: Ticket): Promise<void> {
        try {
            await axios.post<Ticket>(API_URL + '/tickets', ticket);
        } catch (error) {
            console.error(error)
        }
    },

    async createTickets(tickets: Ticket[]): Promise<void> {
        try {
            await axios.post<string>(API_URL + '/tickets/batch', tickets);
        } catch (error) {
            console.error(error)
        }
    },

    async generateTickets(tickets: Ticket[]): Promise<void> {
        try {
            await axios.post<Ticket>(API_URL + '/tickets/generate', tickets);
        } catch (error) {
            console.error(error)
        }
    },

    async updateTicket(ticket: Ticket): Promise<Ticket> {
        try {
            const response = await axios.patch<Ticket>(API_URL + '/tickets/' +
                ticket.ticketNumber, ticket);
            response.data = this.formatTicketForDisplay(response.data);
            return response.data;
        } catch (error) {
            throw error;
        }
    }
};

export default TicketService;
