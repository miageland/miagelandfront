import {Employee} from "../models/employee.ts";
import {Visitor} from "../models/visitor.ts";
import {LoginForm} from "../models/loginForm.ts";
import axios, {AxiosResponse} from "axios";
import {Ref, ref} from "vue";

const API_URL = 'http://localhost:8080';

var authenticatedUser: Ref<Employee | Visitor | null> = ref(null);

const AuthenticationService = {
    async loginEmployee(loginForm: LoginForm) {
        try {
            const response: AxiosResponse<Employee> = await axios.post<Employee>(API_URL + '/login/employee', loginForm);
            authenticatedUser.value = response.data;
            return response.data !== null;
        } catch (error) {
            throw error;
        }
    },

    async loginVisitor(loginForm: LoginForm) {
        try {
            const response: AxiosResponse<Visitor> = await axios.post<Visitor>(API_URL + '/login/visitor', loginForm);
            authenticatedUser.value = response.data;
            return response.data !== null;
        } catch (error) {
            throw error;
        }
    },

    logout() {
        localStorage.removeItem("MiageLand");
        authenticatedUser.value = null;
    },

    isLoggedIn() {
        return authenticatedUser.value !== null;
    },

    getAuthenticatedUser() {
        return authenticatedUser;
    },

    isAuthenticatedUserEmployee() {
        if (authenticatedUser.value !== null) {
            return 'role' in authenticatedUser.value;
        }
        return false;
    }

}
export default AuthenticationService;
