import axios, {AxiosResponse} from 'axios';
import {DailyStats} from "../models/dailyStats.ts";

const API_URL = 'http://localhost:8080';


const DailyStatsService = {
    async getDailyStats(): Promise<DailyStats[]> {
        try {
            const response: AxiosResponse<DailyStats[]> = await axios.get(`${API_URL}/dailyStats`);
            return response.data;
        } catch (error) {
            throw error;
        }
    }
};

export default DailyStatsService;
