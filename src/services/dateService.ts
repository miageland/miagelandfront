export class DateService {
    static formatDateToFrench(date: Date): string {

        const options: Intl.DateTimeFormatOptions = {
            day: '2-digit',
            month: '2-digit',
            year: 'numeric',
        };
        return date.toLocaleDateString('fr-FR', options);
    }

    static convertDateToString(date: Date): string {
        return date.getFullYear() + '-' + String(date.getMonth() + 1).padStart(2, '0') + '-'
            + String(date.getDate()).padStart(2, '0');
    }

    static isDateInNextWeek(date: Date): boolean {
        const today = new Date();
        const nextWeek = new Date();

        nextWeek.setDate(today.getDate() + 7);

        // Check if the date is within the next week
        return date.getTime() > today.getTime() && date.getTime() <= nextWeek.getTime();
    }

    static getYesterdayDate(): Date {
        const date = new Date(); // get current date
        date.setDate(date.getDate() - 1); // subtract one day
        return date;
    }

    static areSameDay(date1: Date, date2: Date): boolean {
        return date1.getFullYear() === date2.getFullYear() &&
            date1.getMonth() === date2.getMonth() &&
            date1.getDate() === date2.getDate();
    }

    static isSameDayOrAfter(date1: Date, date2: Date): boolean {
        const d1 = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
        const d2 = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());

        return d1 >= d2;
    }


    static isDateInPast(date: Date): boolean {
        const today = new Date();
        // Check if the date is in the past
        return date.getTime() < today.getTime();
    }

    static isDateInNextWeekOrPast(date: Date): boolean {
        return this.isDateInNextWeek(date) || this.isDateInPast(date);
    }

    static generateDateRange(startDate: Date, endDate: Date): Date[] {
        const dateArray: Date[] = [];
        let currentDate: Date = new Date(startDate);

        while (currentDate <= endDate) {
            dateArray.push(new Date(currentDate));
            currentDate.setDate(currentDate.getDate() + 1);
        }

        return dateArray;
    }


    static convertToUTC(date: Date): Date {
        let dateInGMTPlus2 = new Date(date);
        return new Date(Date.UTC(
            dateInGMTPlus2.getFullYear(),
            dateInGMTPlus2.getMonth(),
            dateInGMTPlus2.getDate(),
            dateInGMTPlus2.getHours(),
            dateInGMTPlus2.getMinutes(),
            dateInGMTPlus2.getSeconds(),
            dateInGMTPlus2.getMilliseconds()
        ));
    }

    static isDateBetween(date: Date, start: Date, end: Date): boolean {
        return date.getTime() >= start.getTime() && date.getTime() <= end.getTime();
    }

    static isDateInAYear(ticketDate: Date): boolean {
        const tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        const oneYearFromNow = new Date();
        oneYearFromNow.setFullYear(oneYearFromNow.getFullYear() + 1);
        return ticketDate >= tomorrow && ticketDate <= oneYearFromNow;
    }
}
