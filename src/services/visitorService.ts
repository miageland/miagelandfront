import axios, {AxiosResponse} from 'axios';
import {Visitor} from "../models/visitor.ts";
import {LoginForm} from "../models/loginForm.ts";

const API_URL = 'http://localhost:8080';


const visitorService = {


    async getVisitor(id: number): Promise<Visitor> {
        try {
            const response: AxiosResponse<Visitor> = await axios.get(`${API_URL}/visitors/` + id);
            return response.data;
        } catch (error) {
            throw error;
        }
    },
    async getVisitors(): Promise<Visitor[]> {
        try {
            const response: AxiosResponse<Visitor[]> = await axios.get(`${API_URL}/visitors`);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    async loginVisitor(loginForm: LoginForm): Promise<Visitor> {
        try {
            const response: AxiosResponse<Visitor> = await axios.post(`${API_URL}/visitors/login`, loginForm);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    async createVisitor(visitor: Visitor): Promise<void> {
        try {
            await axios.post<Visitor>(API_URL + '/visitors', visitor);
        } catch (error) {
            console.error(error)
        }
    }
};

export default visitorService;
