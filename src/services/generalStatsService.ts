import axios, {AxiosResponse} from 'axios';
import {GeneralStats} from "../models/generalStats.ts";

const API_URL = 'http://localhost:8080';


const GeneralStatsService = {
    async getGeneralStats(): Promise<GeneralStats[]> {
        try {
            const response: AxiosResponse<GeneralStats[]> = await axios.get(`${API_URL}/generalStats`);
            return response.data;
        } catch (error) {
            throw error;
        }
    }
};

export default GeneralStatsService;
