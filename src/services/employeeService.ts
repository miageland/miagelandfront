import axios, {AxiosResponse} from 'axios';
import {Employee} from "../models/employee.ts";
import {Role} from "../models/role.ts";

const API_URL = 'http://localhost:8080';


const employeeService = {
    async getEmployee(id: number): Promise<Employee> {
        try {
            const response: AxiosResponse<Employee> = await axios.get(`${API_URL}/employees/` + id);
            let employee = response.data;
            switch (employee.role) {
                case Role.EMPLOYEE:
                    employee.roleDisplay = "Employé"
                    break;
                case Role.ADMINISTRATOR:
                    employee.roleDisplay = "Administrateur"
                    break;
                case Role.DIRECTOR:
                    employee.roleDisplay = "Directeur"
                    break;
            }
            return employee;
        } catch (error) {
            throw error;
        }
    },

    async getEmployees(): Promise<Employee[]> {
        try {
            const response: AxiosResponse<Employee[]> = await axios.get(`${API_URL}/employees`);
            for (let employee of response.data) {
                switch (employee.role) {
                    case Role.EMPLOYEE:
                        employee.roleDisplay = "Employé"
                        break;
                    case Role.ADMINISTRATOR:
                        employee.roleDisplay = "Administrateur"
                        break;
                    case Role.DIRECTOR:
                        employee.roleDisplay = "Directeur"
                        break;
                }
            }
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    async createEmployee(employee: Employee): Promise<Employee> {
        try {
            const response: AxiosResponse<Employee> = await axios.post<Employee>(API_URL + '/employees', employee);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    async deleteEmployee(employee: Employee): Promise<void> {
        try {
            await axios.delete<Employee>(API_URL + '/employees/' + employee.id);
            return;
        } catch (error) {
            console.error(error)
        }
    },

    async switchRole(employee: Employee): Promise<Employee> {
        try {
            const response = await axios.patch<Employee>(API_URL + '/employees/'
                + employee.id, employee);
            return response.data;
        } catch (error) {
            throw error;
        }
    }
};

export default employeeService;
