import axios, {AxiosResponse} from 'axios';
import {Gauge} from "../models/gauge.ts";

const API_URL = 'http://localhost:8080';

const GaugeService = {
    async getGauge(): Promise<Gauge> {
        try {
            const response: AxiosResponse<Gauge> = await axios.get(`${API_URL}/gauge`);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    async setGauge(gauge: Gauge): Promise<Gauge> {
        try {
            const response: AxiosResponse<Gauge> = await axios.patch(`${API_URL}/gauge`, gauge);
            return response.data;
        } catch (error) {
            throw error;
        }
    }
};

export default GaugeService;
