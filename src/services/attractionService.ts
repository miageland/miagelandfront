import axios, {AxiosResponse} from 'axios';
import {Attraction} from "../models/attraction.ts";

const API_URL = 'http://localhost:8080';


const AttractionService = {
    async getAttractions(): Promise<Attraction[]> {
        try {
            const response: AxiosResponse<Attraction[]> = await axios.get(`${API_URL}/attractions`);
            for (let attraction of response.data) {
                attraction.isOpenedDisplay = attraction.isOpened ? "Ouverte" : "Fermée";
            }
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    async createAttraction(attraction: Attraction): Promise<Attraction> {
        try {
            const response = await axios.post<Attraction>(API_URL + '/attractions', attraction);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    async editAttraction(attraction: Attraction): Promise<Attraction> {
        try {
            const response: AxiosResponse<Attraction> = await axios.patch<Attraction>(API_URL + '/attractions/' + attraction.id, attraction);
            response.data.isOpenedDisplay = response.data.isOpened ? "Ouverte" : "Fermée";
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    async deleteAttraction(attraction: Attraction): Promise<void> {
        try {
            await axios.delete<Attraction>(API_URL + '/attractions/' + attraction.id);
            return ;
        } catch (error) {
            console.error(error)
        }
    }

};

export default AttractionService;
